import React, { Component } from "react";
import { connect } from "react-redux";
import "../App.css";
const urlComponent = "https://image.tmdb.org/t/p/w342";
const movieUrl = "https://www.themoviedb.org/movie/";

class MovieItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="col-sm-12 col-sm-3">
        <div className="thumbnail">
          <a href={movieUrl + this.props.movie.id} target="_blank">
            <img
              src={urlComponent + this.props.movie.poster_path}
              alt={this.props.movie.title + " Image"}
            />
          </a>
          <div className="caption">
            <h3>{this.props.movie.title}</h3>
            <p>Year - {this.props.movie.release_date}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null)(MovieItem);
