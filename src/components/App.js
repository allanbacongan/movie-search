import React, { Component } from "react";
import MovieResults from "./MovieResults";
import { Jumbotron } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="row text-center">
            <div
              className="jumbotron"
              style={{
                backgroundImage:
                  "url(" +
                  "https://png.pngtree.com/thumb_back/fh260/background/20200714/pngtree-modern-double-color-futuristic-neon-background-image_351866.jpg" +
                  ")",
                color: "white",
              }}
            >
              <h1>Welcome,</h1>
              <p>
                Millions of movies, TV shows and people to discover. Explore
                now.
              </p>
            </div>
          </div>
          <div className="row">
            <Router>
              <Switch>
                <Route exact path="/" component={MovieResults} />
              </Switch>
            </Router>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
